import React from 'react';

function Person() {
  return (
    <div className="person">
      <div>
        <img src="http://www.cc.puv.fi/~tka/vamk.png" alt="VAMK Logo" />
        <h2 className="bc">Jere Björs</h2>
        <p></p>
        <span>Opiskelija</span><br />
        <span>Tietotekniikka</span><br />
        <p></p>
        <p></p>
        <br />
        <span>e2101657@edu.vamk.fi</span>
        <br />
        <p></p>
        <span>+358442365985</span>
        <p></p>
        <span>Wolffintie 30, FI-65200 VAASA, Finland</span>
      </div>
    </div>
  );
}

export default Person;